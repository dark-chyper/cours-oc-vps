<?php
/**
 * Created by PhpStorm.
 * User: darkchyper
 * Date: 11/10/2017
 * Time: 19:36
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadUser implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $liste = array(
            array("nom" => "Lhoir",
                    "prenom" => "Simon",
                    "pseudo" => "dark-chyper"),
            array("nom" => "Nebra",
                "prenom" => "Mathieu",
                "pseudo" => "MathieuNebra"),
        );

        foreach($liste as $user){
            $userObject = new User($user["nom"],$user["prenom"],$user["pseudo"]);
            $manager->persist($userObject);
        }
        $manager->flush();
    }
}